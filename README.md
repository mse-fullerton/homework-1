# CPSC 544 Homework 1

- [Project Gantt Chart](https://docs.google.com/spreadsheets/d/1uVsuP1Q9AHaZEeSkVyaU_bOHBvRTgnaTL9WmET6xvPQ)
- [Jira Backlog](https://mse-fullerton.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=C544&view=planning)
- [Homework 1 Outline](https://moodle-2020-2021.fullerton.edu/mod/resource/view.php?id=641514)

## Coding Standards

- All changes to master come in via pull request accepted by at least one reviewer
- Write unit tests to validate logic
- Create regression tests for non-UI bugs


## Current Functionality

- Connect button to function
- Show input value on screen
- Convert field to number input and split input into array
- Get bubble sort working
- Update layout and add instructions

## Functionality to be Added
- Unit tests 
- Regression tests
- Error checking
- Bounds checking
- Update layout and add instructions further
- See HW1 instructions for what else is needed