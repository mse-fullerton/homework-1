package com.example.cpsc544_hw1

import com.example.cpsc544_hw1.sorting.IntBubbleSorterImpl
import com.example.cpsc544_hw1.random.RandomGeneratorImpl
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class BubbleSortUnitTest {
    @Test
    fun sorting_isSuccessful() {
        val numbers: IntArray = intArrayOf(9, 1, 5, 2, 8)
        val testNumbers: IntArray = intArrayOf(1, 2, 5, 8, 9)
        val sorter = IntBubbleSorterImpl(numbers)
        assertEquals(sorter.sort().joinToString(), testNumbers.joinToString())
    }

    @Test
    fun sorting_stepsAreLogged() {
        // known to have only 1 step + initial and sorted == 4
        val numbers: IntArray = intArrayOf(5, 1, 8)
        val sorter = IntBubbleSorterImpl(numbers)
        sorter.sort()
        assertTrue(3 == sorter.steps.count())
    }

    @Test
    fun sorting_onlyInitialAndSortedStepsIfAlreadySorted() {
        val numbers: IntArray = intArrayOf(1, 2, 5, 8, 9)
        val sorter = IntBubbleSorterImpl(numbers)
        sorter.sort()
        // initial + sorted steps only == 2 steps
        assertTrue(2 == sorter.steps.count())
    }

    @Test
    fun random_generateOneValidNumber() {
        val rng = RandomGeneratorImpl()
        val randomResult = rng.generateRandom()
        val resultString = randomResult.toString()

        // 3 <= randomResult <= 8 in length
        assertTrue(resultString.length in 3..8)
    }

    @Test
    fun random_generateMultipleValidNumbers() {
        var count = 0
        repeat(100) {
            random_generateOneValidNumber()
            count++
        }
        assertTrue(count == 100)
        print(count)
        println(" many random_generateMultipleValidNumbers tests passed")
    }

    // Not exactly a Unit Test, but is useful in testing random generator and sorting
    @Test
    fun random_generateOneAndSortIsSuccessful () {
        val rng = RandomGeneratorImpl()
        val randomResult = rng.generateRandom()
        val resultString = randomResult.toString()
        // Turns String to Integers using it to iterate
        val numbers = IntArray(resultString.length) { resultString[it].toInt() }
        val sorter = IntBubbleSorterImpl(numbers)
        sorter.sort()
        assertEquals(sorter.sort().joinToString(), numbers.joinToString())
    }

    @Test
    fun random_generateMultipleAndSortIsSuccessful() {
        var count = 0
        repeat(100) {
            random_generateOneAndSortIsSuccessful()
            count++
        }
        assertTrue(count == 100)
        print(count)
        println(" many random_generateMultipleAndSortIsSuccessful tests passed")
    }

    @Test
    fun ui_sortedValuesAreBoldAndUnderlined() {
        // known to have only 1 step + initial and sorted == 4
        val numbers: IntArray = intArrayOf(5, 1, 8)
        val sorter = IntBubbleSorterImpl(numbers)
        val sort = sorter.sort()
        assertTrue("<u><b>" in sorter.getHtmlSteps() && "</b></u>" in sorter.getHtmlSteps())
    }

}