package com.example.cpsc544_hw1.sorting

const val INLINE_DELIMITER = ","
const val STEP_DELIMITER = "\n<br  />\n"

// TODO make generic rather than integer-specific (not really, but this would probably be valuable in a real project)
interface IIntBubbleSorter {
    val numbers: IntArray
    val steps: ArrayList<String>

    private fun logStep(state:IntArray, swapIndex: Int, loopIteration:Int, isFirstChange:Boolean)
    {
        var result = state.mapIndexed{ index, value ->
            when {
                // swapped high value
                index == swapIndex -> {
                    "<span style=\"color: #8332a8;\"><strong><u><b>$value</b></u></strong></span>"
                }
                // swapped low value (bubbling up)
                index == swapIndex - 1 -> {
                    "<span style=\"color: #c78500;\"><strong><u><b>$value</b></u></strong></span>"
                }
                // known sorted values
                index < loopIteration -> {
                    "<span style=\"color: red;\">$value</span>"
                }
                // normal value
                else -> {
                    value
                }
            }
        }.joinToString(INLINE_DELIMITER)
        if (isFirstChange)
        {
            result = "$result &larr; loop ${loopIteration+1}"
        }
        steps.add(result)
    }

    private fun logInitial()
    {
        steps.add("<strong>${numbers.joinToString ( INLINE_DELIMITER )}</strong> <strong>&larr; initial value</strong>")
    }

    private fun logFinal()
    {
        steps.add("<span style=\"color:red;\"><strong>${numbers.joinToString (INLINE_DELIMITER )}</strong> <strong>&larr; sorted value</strong></span>")
    }

    fun getHtmlSteps():String
    {
        return steps.joinToString(STEP_DELIMITER)
    }

    private fun swap(right: Int, left: Int)
    {
        val temp = numbers[right]
        numbers[right] = numbers[left]
        numbers[left] = temp
    }

    fun sort():IntArray{
        if (steps.isNotEmpty()){
            return numbers
        }
        var swap = true
        var loop = 0
        logInitial()
        while(swap){
            swap = false
            var isFirstChange = true
            for(i in numbers.size-1 downTo 1){
                if(numbers[i] < numbers[i-1]){
                    swap(i, i-1)
                    swap = true
                    // log each change
                    logStep(numbers, i, loop, isFirstChange)
                    isFirstChange = false
                }
            }
            loop++
        }
        logFinal()
        return numbers
    }
}