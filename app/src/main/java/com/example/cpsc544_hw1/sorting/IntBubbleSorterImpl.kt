package com.example.cpsc544_hw1.sorting

class IntBubbleSorterImpl(override val numbers: IntArray): IIntBubbleSorter {
    override val steps = ArrayList<String>()
    init {
        if (numbers.size < 3) {
            throw Exception("You must enter at least 3 digits. You entered ${numbers.size} digits")
        }
        if (numbers.size > 8)
        {
            throw Exception("You cannot enter more than 8 digits. You entered ${numbers.size} digits.")
        }
    }
}
