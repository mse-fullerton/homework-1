package com.example.cpsc544_hw1.random

const val MAX_INPUT = 99999999
const val MIN_INPUT = 100

// Digit Subgroups
const val SMALL_DIGITS_MAX = 99999 	// 3, 4, 5 digits
const val MEDIUM_DIGITS_MAX = 9999999	// 6, 7 digits

interface IRandomGenerator {

    //val test: Int

    fun generateRandom() : Int {
        var randomNum: Int = 0
        var currTime: Long = 0

        currTime = System.currentTimeMillis()%3.toLong()
        //println("Time div: " + currTime)

        val sorter = RandomGeneratorImpl()

        // 3 Subgroups Based on Number of Digits
        when(currTime.toInt()) {
            // First third
            0 -> randomNum = (MIN_INPUT..SMALL_DIGITS_MAX).random()
            // Middle third
            1 -> randomNum = (SMALL_DIGITS_MAX..MEDIUM_DIGITS_MAX).random()
            // Top Third
            2 -> randomNum = (MEDIUM_DIGITS_MAX..MAX_INPUT).random()
        }

        return randomNum
    }
}