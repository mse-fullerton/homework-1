package com.example.cpsc544_hw1

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.example.cpsc544_hw1.random.RandomGeneratorImpl
import com.example.cpsc544_hw1.sorting.IntBubbleSorterImpl
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonExit.setOnClickListener {
            this.finishAffinity()
        }
        buttonSort.setOnClickListener{
            sortNumbers()
        }
        buttonRandom.setOnClickListener {
            generateRandomNumbers()
        }
        buttonInstructions.setOnClickListener{
            val intent = Intent(this,OverviewPage::class.java)
            startActivity(intent)
        }
    }

    private fun sortNumbers() {
        val numberInput = findViewById<EditText>(R.id.inputNumbers)
        val numbersArray = numberInput.text.split("").mapNotNull { it.toIntOrNull() }.toIntArray()
        val textField = findViewById<TextView>(R.id.sortedNumbersTextView)
        try {
            val sorter = IntBubbleSorterImpl(numbersArray)
            sorter.sort()
            textField.text = HtmlCompat.fromHtml(sorter.getHtmlSteps(), 0)
            numberInput.text.clear()
        } catch (e: Exception) {
            // show the error it on the screen
            textField.text = HtmlCompat.fromHtml(e.message!!, 0)
        }

    }

    private fun generateRandomNumbers(): String {

        val numberInput = findViewById<EditText>(R.id.inputNumbers)
        val textField = findViewById<TextView>(R.id.sortedNumbersTextView)
        var randomResult = 0

        try {
            val rng = RandomGeneratorImpl()
            randomResult = rng.generateRandom()
            numberInput.setText(randomResult.toString())

            /* Debug
            print(randomResult)
            println(" Size:" + (randomResult).toString().length)
            */
        } catch (e: Exception) {
            // Screams
            textField.text = HtmlCompat.fromHtml(e.message!!, 0)
        }

        return randomResult.toString()

    }
}